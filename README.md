# NEAD Cloud configuration

Nextcloud configuration based on the guides of Carsten Rieger:
https://www.c-rieger.de/category/nextcloud/

it assumes ubuntu 18.04 with nginx and php-fpm (7.x)

letsencrypt certificates are automatically updated using certbot (see etc/cron.d/certbot)

files for the acme challenge are served using a separate vhost which is available on localhost:81 (see etc/nginx/sites-available/letsencrypt)

the default vhost passes all http traffic for /.well-known/acme-challenge to that host and redirects all other traffic to https.

the nginx/sites-available/office vhost should work with onlyoffice as well as collaboraoffice, as long as the respective docker container is connected to localhost:9980.